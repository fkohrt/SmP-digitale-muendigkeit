# Speaker

## HIIG
- Direktoren
  - [Prof. Dr. Björn Scheuermann](https://www.hiig.de/staff/bjoern-scheuermann/), Vertrauensdozent
- Forschungsteam
  - [Dr. Christian Aissa Djeffal](https://www.hiig.de/staff/christian-djeffal/), Alumnus, Rechtswissenschaft
- Assoziierte ForscherInnen
  - [Dr. Christian Pentzold](https://www.hiig.de/staff/christian-pentzold/), Alumnus, Medien und Kommunikation, Film & Media Studies, Social Science of the Internet
  - [Henrike Marie Maier](https://www.hiig.de/staff/rike-maier/), Alumna, Rechtswissenschaft
  - [Uta Meier-Hahn](https://www.hiig.de/staff/uta-meier-hahn/), hat [Internet Policy Review](https://policyreview.info/) mit aufgebaut
  - [Lisa Gutermuth](https://www.hiig.de/staff/lisa-gutermuth/), arbeitet bei [Ranking Digital Rights](https://rankingdigitalrights.org/)

## Tactical Technology Collective

## Bits & Bäume

- [Workshop: Die Digitalisierungsgesellschaft](https://fahrplan.bits-und-baeume.org/events/137.html) _Welche Auswirkungen hat Digitalisierung auf kapitalistische Wohlstandsgesellschaften?_
  - [Nina Treu](https://fahrplan.bits-und-baeume.org/speakers/346.html)
- [Workshop: Kampagnen-Brainstorming: Facebook zerschlagen?](https://fahrplan.bits-und-baeume.org/events/77.html)
  - **[Constanze Kurz](http://gewissensbits.gi.de/constanze-kurz/)**, hat ein Buch zur [digitalen Mündigkeit](http://datenfresser.info/?page_id=62) geschrieben, auch Lightning Talk _Facebook zerschlagen, aber wie?_
  - [Tilman Santarius](https://fahrplan.bits-und-baeume.org/speakers/310.html)
- [Lightning Talk: Sporangium](https://fahrplan.bits-und-baeume.org/events/249.html)
  - [Katika Kühnreich](https://fahrplan.bits-und-baeume.org/speakers/398.html) _NO FREEDOMS LEFT, GAME OVER? – Das Spiel mit den Social-Credit-Scores_
  - **[Benjamin Kees](https://fahrplan.bits-und-baeume.org/speakers/62.html)** _Automatisierte Videoüberwachung. Von Computern, die auf Menschen starren_
  - [Arne Semsrott](https://fahrplan.bits-und-baeume.org/speakers/29.html), _Von Transparenz und anderen Hürden der Demokratie_
- [Podiumsdiskussion: Von Tech-Monopolen und IT-Giganten: Kritik des digitalen Kapitalismus](https://fahrplan.bits-und-baeume.org/events/242.html)
  - **[Frank Rieger](https://fahrplan.bits-und-baeume.org/speakers/441.html)**, hat mit Constance Kurz ein Buch geschrieben, Sprecher CCC
- [Workshop: Ein Blick hinters Smartphone
](https://fahrplan.bits-und-baeume.org/events/115.html) _Wie der Einstieg in die Hardware in unseren Taschen gelingen kann._
  - [Maximilian Voigt](https://fahrplan.bits-und-baeume.org/speakers/235.html) vom [FabLab Cottbus](http://blog.fablab-cottbus.de/2018/11/19/lasst-uns-die-beziehungskriese-mit-technik-ueberwinden/)
- [Andere: Philosophischer Salon: Wie digital wird eine wirklich zukunftsfähige Gesellschaft sein?](https://fahrplan.bits-und-baeume.org/events/247.html)
  - [Harald Welzer](https://fahrplan.bits-und-baeume.org/speakers/390.html) von der [Stiftung Futurzwei](https://futurzwei.org/)
- [Andere: The Glass Room Experience](https://fahrplan.bits-und-baeume.org/events/100.html) _Looking into your online life_
  - [Caroline Kent](https://fahrplan.bits-und-baeume.org/speakers/196.html), **Tactical Technology Collective**
- [Workshop: Von der imperialen zur konvivialen Technik](https://fahrplan.bits-und-baeume.org/events/142.html)

## FIfFKon
- **[Rainer Mühlhoff](https://rainermuehlhoff.de/)**, _Digitale Entmündigung und „UserExperience Design“. Über Nudging, Tracking und Infantilisierung im Netz_

## Forum Privatheit
- [Tagung „Zukunft der Datenökonomie“](http://www.forum-privatheit.de/forum-privatheit-de/aktuelles/veranstaltungen/veranstaltungen-des-forums/vergangene-Veranstaltungen/Jahreskonferenz-Zukunft-der-Datenoekonomie.php)
- [Die Fortentwicklung des Datenschutzes](http://www.forum-privatheit.de/forum-privatheit-de/aktuelles/veranstaltungen/veranstaltungen-des-forums/vergangene-Veranstaltungen/forum-privatheit_konferenz-2017.php)
  - **[Dr. Thilo Hagendorff](https://www.thilo-hagendorff.info/)**

## Strukturwandel des Privaten
[Link](https://strukturwandeldesprivaten.wordpress.com/)
- [Dr. Tobias Dienlin](https://strukturwandeldesprivaten.wordpress.com/team/tobias-dienlin/), Alumnus, [Dissertation „The Psychology of Privacy“](http://opus.uni-hohenheim.de/volltexte/2017/1315/)

## Weitere
- Man darf ja noch träumen
  - [Christian Fuchs](http://fuchs.uti.at/)
  - [Shoshana Zuboff](http://shoshanazuboff.com/)
- ohne Kategorie
  - [Leena Simon](https://leena.de/digitale-muendigkeit/), [muendigkeit.digital](https://muendigkeit.digital/)
  - [Prof. Dr. Christian Schicha](http://www.schicha.net/)
  - Jérémie Zimmermann von [La Quadrature du Net](https://www.laquadrature.net/en), lebt in Berlin und hat schon einmal einen Vortrag bei der Heinrich-Böll-Stiftung gehalten ([Quelle](https://de.wikipedia.org/wiki/La_Quadrature_du_Net#/media/File:J%C3%A9r%C3%A9mie_Zimmermann_crop.jpg))
  - Tobias Keber vom [Institut für digitale Ethik](https://www.digitale-ethik.de/institut/leitung/), hat auch schon [Sommerakademie-Workshops](https://www.digitale-ethik.de/forschung/vortraege/) geleitet