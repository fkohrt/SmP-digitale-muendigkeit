# SmP Digitale Mündigkeit

## Aktuelles

**Dieses Projekt liegt momentan auf Eis und wird nicht bearbeitet.**

## Beschreibung

> Wir wollen im Rahmen von „Stipendiatinnen und Stipendiaten machen Programm“ der Studienstiftung des deutschen Volkes ein mehrtägiges Seminar zum Thema digitale Mündigkeit / Selbstverteidigung planen. Dieses Projekt soll unser Vorhaben dokumentieren.

- [Brainstorming](Brainstorming.md)
- [Eckpfeiler](Eckpfeiler.md)

## Planung

- [Checkliste](Checkliste.md)