# Veranstaltungsort
## Unterkunft + Tagung
- [Jugendherberge Berlin-Ostkreuz](https://www.jugendherbergeberlinostkreuz.de/): 27. 6. 2019 bis 30. 6. 2019
- [Jugendherberge Berlin-Wannsee](http://www.jh-wannsee.de/): 25. 7. 2019 bis 28. 7. 2019, 6 Tagungsräume
- [St.-Michaels-Heim](https://www.st-michaels-heim.de/): [Zimmer](https://www.st-michaels-heim.de/jugendgaestehaus/zimmer.html), [Veranstaltungsräume](https://www.st-michaels.de/bankett/veranstaltungsraeume.html), [Lageplan (pdf)](https://www.st-michaels-heim.de/media/Dokumente/lageplan_A4_ohne_standort.pdf)
  - [Restaurant](https://www.st-michaels.de/restaurant.html): Mo bis So, 7 bis 15 Uhr
  - [Biergarten](https://biergarten-am-herthasee.eatbu.com/): Mo bis So, 12 bis 22 Uhr
  - [Catering](http://deli-gate.de/kongress-tagung/)
- Teikyo Hotel und Jugendgästehaus am Zeuthener See: 86 [Betten](https://www.teikyo-berlin.com/newpage1), 9 [Tagungsräume](https://www.teikyo-berlin.com/tagungen)
- Tagungshaus Alte Feuerwache: ~60 [Betten](http://www.tagungshaus-af.de/index.php/26.html), 5 [Seminarräume](http://www.tagungshaus-af.de/index.php/seminarmoeglichkeiten-44.html)
- [CVJM Jugendgästehaus](http://www.cvjm-jugendgaestehaus.de/)

## Tagung
- [Umwelt-Bildungszentrum Berlin](http://www.umwelt-bildungszentrum.de/anfrage/)
- [Tagen in Berlin](https://www.tagen-in-berlin.de/tagungsraeume/gruppenraeume)

## Bemerkungen
- alle Berliner JuHes auf [gruppenhaus.de](https://www.gruppenhaus.de/) durchgesehen
- Verworfene
  - [Jugenddorf Berlin](http://www.jugenddorf-berlin.de/) (schlechte Lage + zu wenig Seminarräume)
  - [bbz](http://www.bbz.verdi.de/) (zu teuer)
  - [Nordufer](https://www.jugendgaestehausberlin.de/) (nur ein Seminarraum + drei kl. Gruppenräume)
  - [City Hostel Berlin](https://www.cityhostel-berlin.com/tagungsraum_berlin_mitte.aspx) (anonym)
  - [Happy Bed](https://www.happybed.de/gruppen-2/) (anonym)